package net.masalachai.sample.epp;

import java.util.Vector;

import com.verisign.epp.codec.domain.EPPDomainCheckResp;
import com.verisign.epp.interfaces.EPPApplicationSingle;
import com.verisign.epp.interfaces.EPPSession;
import com.verisign.epp.namestore.interfaces.NSDomain;
import com.verisign.epp.namestore.interfaces.NSSubProduct;
import com.verisign.epp.pool.EPPSessionPool;

public class App {
    public static void main(String[] args) throws Exception {
        EPPApplicationSingle.getInstance().initialize("epp-client.config");
        EPPSessionPool.getInstance().init();

        EPPSession session = EPPSessionPool.getInstance().borrowObject();
        NSDomain nsDomain = new NSDomain(session);
        nsDomain.setTransId("transaction-id");
        nsDomain.addDomainName("epptestdomain.com");
        nsDomain.setSubProductID(NSSubProduct.COM);
        EPPDomainCheckResp response = nsDomain.sendCheck();

        EPPSessionPool.getInstance().returnObject(session);

        System.out.println(String.valueOf(response));
    }
}
