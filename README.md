## Example EPP Client using Verisign SDK

This is a example of an epp client application that uses the verisign-sdk to connect to a registry and run a domain check transaction.

## Dependencies

- Java 11 or higher
- Maven 3.6.3 or higher

## Operation

### Checkout

```bash
git clone https://codeberg.org/ritesh/epp-example
```

### Create a JKS keystore in the project dir

I have only tested the verisign sdk with a JKS (java format) keystore. If you already have a PKCS12 keystore containing your private key and client certificate for the registry, you can run the following command to convert it to the JKS format

```bash
keytool -importkeystore -srckeystore <pkcs12 keystore file> -srcstoretype pkcs12 -destkeystore keystore.jks -deststoretype JKS
```

### Update the epp-client.config file for your connection

Update these following values in the config file for your connection with the registry

```
# Registry's EPP server hostname
EPP.ServerName=epp-ote.verisign-grs.com
# Registry's EPP Port
EPP.ServerPort=700
# Client keystore format (JKS in this case)
EPP.SSLKeyStore=JKS
# Client keystore file path (absolute or relative to your execution path)
EPP.SSLKeyFileName=keystore.jks
# The passphrase for your client keystore
EPP.SSLPassPhrase=passphrase
# Your EPP login username
EPP.SessionPool.clientId=username
# Your EPP login password
EPP.SessionPool.password=password
# Change to all if you want to see full SSL debugging
javax.net.debug=none
```

You can also change some of the other values depending on your registry and planned setup.

The verisign sdk also comes with its own connection pool which can be configured from the config file.

### Build and run

You can build and run the application after this.

```bash
mvn clean package
java -jar target/epp-example-1.0-SNAPSHOT-jar-with-dependencies.jar
```

You should see a domain check for the hardcoded domain `epptestdomain.com` being run and you should receive an EPP XML response similar to the following.

```xml
<response xmlns="urn:ietf:params:xml:ns:epp-1.0">
  <result code="1000">
    <msg>Command completed successfully</msg>
  </result>
  <resData>
    <domain:chkData xmlns:domain="urn:ietf:params:xml:ns:domain-1.0">
      <domain:cd>
        <domain:name avail="0">epptestdomain.com</domain:name>
        <domain:reason>Domain exists</domain:reason>
      </domain:cd>
    </domain:chkData>
  </resData>
  <trID>
    <clTRID>transaction-id</clTRID>
    <svTRID>RO-543-1660827989294393</svTRID>
  </trID>
</response>
```

If you want to check another domain, you can modify the domain name in `App.java`.